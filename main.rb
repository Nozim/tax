require_relative 'lib/parser'
require_relative 'lib/goods'
require_relative 'lib/goods_item'

input = "1, book, 12.49
        1, music CD, 14.99
        1, chocolate bar, 0.85"

goods = Parser.parse(input)

puts goods.receipt 
