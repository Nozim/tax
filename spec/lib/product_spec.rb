require 'spec_helper'

describe Product do
  before :each do
    @item = described_class.new '1', 'book', '2.4', [:exempted]
  end

  describe '#new' do
    it 'takes three parameters and returns Product object' do
      expect(@item).to be_an_instance_of Product
    end
  end

  describe '#quantity' do
    it 'returns correct quantity' do
      expect(@item.quantity).to eq 1
    end
  end

  describe '#name' do
    it 'returns correct name' do
      expect(@item.name).to eq 'book'
    end
  end

  describe '#price' do
    it 'returns correct price' do
      expect(@item.price).to eq 2.4
    end
  end

  describe '#categories' do
    it 'returns correct categories' do
      expect(@item.categories).to eq [:exempted]
    end
  end

  describe '#total_taxed_price' do
    it 'returns correct taxed price' do
      expect(@item.total_taxed_price).to eq 2.4
    end
  end

  describe '#total_tax' do
    it 'returns total tax amount' do
      expect(@item.total_tax).to eq 0.0
    end
  end

  context 'regular goods' do
    before :each do
      @item = described_class.new '1', 'music CD', '14.99', [:regular]
    end

    it 'calculates total taxed price correctly' do
      expect(@item.total_taxed_price).to eq 16.49
    end

    it 'calculates total tax amount correctly' do
      expect(@item.tax_sum).to eq 1.5
    end

    context 'impored' do
      before :each do
        @item = described_class.new '1',
                                    'imported bottle of perfume',
                                    '47.50',
                                    [:regular, :imported]
      end

      it 'calculates total taxed price correctly' do
        expect(@item.total_taxed_price).to eq 54.65
      end

      it 'calculates total tax amount correctly' do
        expect(@item.tax_sum).to eq 7.15
      end
    end
  end

  describe '#print_details' do
    before :each do
      @item = described_class.new '1',
                                  'imported bottle of perfume',
                                  '47.50',
                                  [:regular, :imported]
    end

    it "contains item's details" do
      expect(
        @item.receipt_details
      ).to eq '1, imported bottle of perfume, 54.65'
    end
  end
end
