require 'spec_helper'

describe Parser do
  before :all do
    @input = "1, book, 12.49\n" \
    "1, music CD, 14.99\n" \
    '1, imported chocolate bar, 0.85'
    @goods = described_class.parse(@input)
  end
  describe '.parse' do
    it 'creates goods object from input' do
      expect(@goods).to be_instance_of Goods
    end

    it 'creates correct number of items' do
      expect(@goods).to have(3).items
    end

    it 'populates attributes properly' do
      expect(@goods.get_item('book').price).to eq 12.49
    end

    it 'detects type of the goods' do
      expect(
        @goods.get_item('book').categories
      ).to eq [:exempted]

      expect(
        @goods.get_item('music CD').categories
      ).to eq [:regular]

      expect(
        @goods.get_item('imported chocolate bar').categories
      ).to eq [:exempted, :imported]
    end
  end
end
