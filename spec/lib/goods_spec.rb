require 'spec_helper'

describe Goods do
  before :each do
    @goods = Goods.new 'goods.yml'
  end

  describe '#new' do
    context 'with no parameters' do
      it 'has no items' do
        goods = Goods.new
        expect(goods).to have(0).items
      end
    end

    context 'with yaml file as parameter' do
      it 'has 3 books' do
        expect(@goods).to have(3).items
      end
    end
  end

  it 'returns all the items of given category' do
    expect(@goods.get_items_in_category(:exempted).length).to eq 2
  end

  it 'accepts new items' do
    @goods.add_item(Product.new('1', 'bottle of perfume', '3.1', [:regular]))
    expect(@goods.get_item('bottle of perfume')).to be_instance_of Product
  end

  it 'can be saved to file' do
    items_names = @goods.items.map(&:name)
    @goods.save 'goods2.yml'
    goods2 = Goods.new 'goods2.yml'
    items_names2 = goods2.items.map(&:name)
    expect(items_names2).to eq items_names
  end

  describe '#receipt' do
    context 'imported goods' do
      before :each do
        @goods = Goods.new
        @goods.add_item(
          Product.new 1,
                        'imported box of chocolates',
                        10.00,
                        [:exempted, :imported]
        )

        @goods.add_item(
          Product.new 1,
                        'imported bottle of perfume',
                        47.50,
                        [:regular, :imported]
        )
      end

      it 'receipt' do
        receipt = @goods.receipt
        expect(receipt).to eq "Quantity, Product, Price\n" \
            "1, imported box of chocolates, 10.50\n" \
            "1, imported bottle of perfume, 54.65\n" \
            "\n" \
            "Sales Taxes: 7.65\n" \
            'Total: 65.15'
      end
    end

    context 'imported and exempted goods' do
      before :each do
        @goods = Goods.new
        @goods.add_item(
          Product.new 1,
                        'imported bottle of perfume',
                        27.99,
                        [:regular, :imported]
        )
        @goods.add_item Product.new 1, 'bottle of perfume', 18.99, [:regular]
        @goods.add_item Product.new 1, 'packet of headache pills', 9.75, [
          :exempted
        ]
        @goods.add_item Product.new 1,
                                      'box of imported chocolates',
                                      11.25,
                                      [:exempted, :imported]
      end

      it 'prints receipt' do
        receipt = @goods.receipt
        expect(receipt).to eq "Quantity, Product, Price\n" \
         "1, imported bottle of perfume, 32.19\n" \
         "1, bottle of perfume, 20.89\n" \
         "1, packet of headache pills, 9.75\n" \
         "1, box of imported chocolates, 11.85\n" \
         "\n" \
         "Sales Taxes: 6.70\n" \
         'Total: 74.68'
      end
    end
  end
end
