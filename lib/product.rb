# Public: Object for storing product details
#
# quantity - number of items of given goods
# name - name of product 
# price - sale price without tax for one item
# categories - array of symbols that represents type of product,
#  can contain either :regular or :exempted and :imported
#
# Examples
# Product.new '1', 'book', '2.4', [:exempted, :imported]
class Product
  RATES = { regular: 0.1, exempted: 0.0, imported: 0.05 }
  ROUNDING_FACTOR  = 0.05

  attr_accessor :quantity, :name, :price, :categories

  def initialize(quantity, name, price, categories)
    @quantity = quantity.to_i
    @name = name
    @price = price.to_f
    @categories = categories
  end

  def receipt_details
    sprintf('%d, %s, %.2f', @quantity, @name, total_taxed_price)
  end

  def total_taxed_price
    (total_price + tax_sum).round(2)
  end

  def tax_sum
    round(total_price * total_tax)
  end

  def total_tax
    @categories.inject(0) { |a, e| a + RATES[e] }.round(2)
  end

  def total_price
    @price * @quantity
  end

  def round(value)
    (value / ROUNDING_FACTOR).ceil * ROUNDING_FACTOR
  end
end
