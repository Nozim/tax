# Public: Goods container and receipt printer
#
# goods_file - path to file with serialized goods data
#
# Examples
# Goods.new "/path/to/file"
class Goods
  attr_accessor :items

  def initialize(goods_file = false)
    @goods_file = goods_file
    @items = @goods_file ? YAML.load(File.read(@goods_file)) : []
  end

  def get_items_in_category(category)
    @items.select { |item| item.categories.include? category }
  end

  def add_item(item)
    @items.push item
  end

  def get_item(name)
    @items.select { |item| item.name == name }.first
  end

  def save(goods_file = false)
    @goods_file = goods_file || @goods_file || 'goods.yml'

    File.open(@goods_file, 'w') do |f|
      f.write YAML.dump @items
    end
  end

  def receipt
    items_list = items.inject('') { |a, e| a + e.receipt_details + "\n" }
    total_tax = items.inject(0) { |a, e| a + e.tax_sum }
    total_price = items.inject(0) { |a, e| a + e.total_taxed_price }
    sprintf("Quantity, Product, Price\n%s\nSales Taxes: %.2f\nTotal: %.2f",
            items_list,
            total_tax,
            total_price
           )
  end
end
