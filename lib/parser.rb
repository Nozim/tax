# Public: Creating Goods object from string
#
# input - goods in string representation
#
# Examples
# Parser.parse "1, book, 3.4"
class Parser
  def self.parse(input)
    goods = Goods.new
    fields_array = input.split("\n").map { |line| line.split(',').map(&:strip) }
    fields_array.each { |arr| goods.add_item(build_item(arr)) }
    goods
  end

  def self.build_item(array)
    qty, name, price = array
    categories = [if name.match(/book|chocolate|pills/).nil?
                    :regular
                  else
                    :exempted
                  end
                 ]
    categories.push(:imported) if name.include? 'imported'
    Product.new qty, name, price, categories
  end
end
